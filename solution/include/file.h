#ifndef SECTION_EXTRACTOR_FILE_H
#define SECTION_EXTRACTOR_FILE_H

#include <stdint.h>

struct PEFile read_PE_file(FILE *in);
void write_section(FILE *out, const uint8_t *section, uint32_t size);

#endif //SECTION_EXTRACTOR_FILE_H
