#ifndef SECTION_EXTRACTOR_PE_H
#define SECTION_EXTRACTOR_PE_H

#include <stdint.h>

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
/// Structure containing PE header data (COFF File Header)
struct
#if defined __GNUC__
__attribute__((packed))
#endif
PEHeader {
    /// The number that identifies the type of target machine
    uint16_t Machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers.
    uint16_t NumberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
    uint32_t TimeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t NumberOfSymbols;
    /// The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file.
    uint16_t SizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file. For specific flag values, see Characteristics.
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/// Structure containing optional header data (not used)
struct OptionalHeader {
    /// Temp stub
    uint8_t stub;
};

#if defined _MSC_VER
#pragma pack(push, 1)
#endif
/// Structure containing section header data
struct
#if defined __GNUC__
        __attribute__((packed))
#endif
SectionHeader {
    /// An 8-byte, null-padded UTF-8 encoded string.
    uint8_t  Name[8];
    /// The total size of the section when loaded into memory. If this value is greater than SizeOfRawData, the section is zero-padded. This field is valid only for executable images and should be set to zero for object files.
    uint32_t VirtualSize;
    /// The address of the first byte of the section relative to the image base when the section is loaded into memory.
    uint32_t VirtualAddress;
    /// The size of the section.
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file.
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers. This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t PointerToLinenumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
    uint16_t NumberOfLinenumbers;
    /// The flags that describe the characteristics of the section.
    uint32_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif

/// Structure containing PE file data
struct PEFile {
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to an optional header
    uint32_t optional_header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;

    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Optional header
    struct OptionalHeader optional_header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;

    ///@}
};

struct SectionHeader find_section_by_name(struct PEFile file, char *name);
uint8_t * get_section_data(FILE *in, struct SectionHeader header);

#endif //SECTION_EXTRACTOR_PE_H
