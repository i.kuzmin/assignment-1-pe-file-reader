/// @file 
/// @brief Main application file

#include <stdio.h>
#include <stdlib.h>

#include "file.h"
#include "PE.h"

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test.
/// @param[in] f File to print to (e.g., stdout).
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Free resources.
/// @param in Input file.
/// @param out Output file.
/// @param file PEFile struct.
/// @param section Section data byte buffer.
void free_resources(FILE *in, FILE *out, struct PEFile file, uint8_t *section) {
    if (in) {
        fclose(in);
    }
    if (out) {
        fclose(out);
    }
    if (file.section_headers) {
        free(file.section_headers);
    }
    if (section) {
        free(section);
    }
}

/// @brief Application entry point.
/// @param[in] argc Number of command line arguments.
/// @param[in] argv Command line arguments.
/// @return 0 in case of success or error code.
int main(int argc, char **argv) {
    if (argc != 4) {
        usage(stdout);
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (!in) {
        return 1;
    }

    struct PEFile file = read_PE_file(in);
    struct SectionHeader section_header = find_section_by_name(file, argv[2]);
    uint8_t *section = get_section_data(in, section_header);

    FILE *out = fopen(argv[3], "wb");
    if (!out) {
        free_resources(in, out, file, section);
        return 1;
    }

    write_section(out, section, section_header.SizeOfRawData);

    free_resources(in, out, file, section);
    return 0;
}
