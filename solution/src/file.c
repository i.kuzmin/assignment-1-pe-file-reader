/// @file
/// @brief File handle file.

#include <stdio.h>
#include <stdlib.h>

#include "PE.h"

/// Offset to PE signature.
#define MAIN_OFFSET 0x3c

/// @brief Read PE file.
/// @details Read PE file to struct.
/// @param in Given file.
/// @return Read PEFile struct.
struct PEFile read_PE_file(FILE *in) {
    struct PEFile file = (struct PEFile) {0};

    fseek(in, MAIN_OFFSET, SEEK_SET);
    fread(&file.header_offset, 4, 1, in);
    fseek(in, file.header_offset, SEEK_SET);
    fread(&file.magic, sizeof(uint32_t), 1, in);
    fread(&file.header, sizeof(struct PEHeader), 1, in);

    fseek(in, file.header.SizeOfOptionalHeader, SEEK_CUR);

    file.section_headers = malloc(sizeof(struct SectionHeader) * file.header.NumberOfSections);
    fread(file.section_headers, sizeof(struct SectionHeader), file.header.NumberOfSections, in);

    return file;
}

/// @brief Write section.
/// @details Write section  to file.
/// @param out Given file.
/// @param section Byte buffer of data.
/// @param size Size of buffer.
void write_section(FILE *out, const uint8_t *section, uint32_t size) {
    fwrite(section, size, 1, out);
}
