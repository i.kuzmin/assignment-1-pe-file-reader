/// @file
/// @brief PE operations file.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "PE.h"

/// @brief Find min.
/// @details Find min between 2 size_t variables.
/// @param a First variable.
/// @param b First variable.
/// @return Min value.
static inline size_t min(size_t a, size_t b) {
    if (a < b)
        return a;
    return b;
}

/// @brief Find section by name.
/// @details Find section in PEFile by name.
/// @param file PEFile structure.
/// @param name Name of the section.
/// @return SectionHeader of the found section
struct SectionHeader find_section_by_name(struct PEFile file, char *name) {
    for (size_t i = 0; i < file.header.NumberOfSections; i++) {
        if (memcmp(file.section_headers[i].Name, name, min(strlen(name), 8)) == 0) {
            return file.section_headers[i];
        }
    }

    return (struct SectionHeader) {0};
}

/// @brief Get section data.
/// @details Get section data from file.
/// @param in File to search.
/// @param header SectionHeader of the section.
/// @return Byte buffer of the data
uint8_t * get_section_data(FILE *in, struct SectionHeader header) {
    if (header.PointerToRawData == 0) {
        return NULL;
    }

    fseek(in, header.PointerToRawData, SEEK_SET);

    uint8_t *section = malloc(header.SizeOfRawData);
    if (section != NULL) {
        fread(section, header.SizeOfRawData, 1, in);
    }

    return section;
}
